#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <WS2tcpip.h>
#pragma comment(lib,"ws2_32.lib")

using namespace std;

int main() {
	string ipAddress = "127.0.0.1";	//localhost
	int port = 5577;

	//Initialize WinSock
	WSAData data;
	WORD ver = MAKEWORD(2, 2);
	int wsResult = WSAStartup(ver, &data);
	if (wsResult != 0)
	{
		cerr << "Can't start Winsock, Err #" << wsResult << endl; 
		return 1;
	}

	//Create socket
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		cerr << "Can't create socket, Err #" << WSAGetLastError() << endl;
		WSACleanup();
		return 1;
	}

	//Fill in a hint structure
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	inet_pton(AF_INET, ipAddress.c_str()/*const char * */, &hint.sin_addr);

	//Connect to server
	int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
	if (connResult == SOCKET_ERROR)
	{
		cerr << "Can't connect to server, Err #" << WSAGetLastError() << endl;
		closesocket(sock);
		WSACleanup();
		return 1;
	}



	//first usage
	POINT p;
	GetCursorPos(&p);
	int pSavedX = p.x, pSavedY = p.y;
	string startingMessage = "Started sending mouse data:\n";
	int sendMessage = send(sock, startingMessage.c_str(), startingMessage.size() + 1, 0);
	if (sendMessage == SOCKET_ERROR)
	{
		cerr << "Can't send data to server, Err #" << WSAGetLastError() << endl;
		closesocket(sock);
		WSACleanup();
		return 1;
	}

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));//60000
		GetCursorPos(&p);
		if (pSavedX != p.x || pSavedY != p.y)
		{
			//�������� ���������� "Active"
			int sendMessage = send(sock, "Active\n", 8, 0);
			pSavedX = p.x, pSavedY = p.y;
			cout << "p.x = " << p.x << "	p.y = " << p.y << endl;
			if (sendMessage == SOCKET_ERROR)
			{
				cerr << "Can't send data to server, Err #" << WSAGetLastError() << endl;
				closesocket(sock);
				WSACleanup();
				return 1;
			}
		}
		else
		{
			//�������� ���������� "Passive"
			int sendMessage = send(sock, "Passive\n", 9, 0);
			pSavedX = p.x, pSavedY = p.y;
			cout << "p.x = " << p.x << "	p.y = " << p.y << endl;
			if (sendMessage == SOCKET_ERROR)
			{
				cerr << "Can't send data to server, Err #" << WSAGetLastError() << endl;
				closesocket(sock);
				WSACleanup();
				return 1;
			}
		}
	}

	//Close
	closesocket(sock);
	WSACleanup();

	return 0;
}